/* eslint-disable */
import Crypto from 'crypto-js';
import Axios from 'axios';
import apiConfigs from '@/api/configs.json';
import store from '@/store/store';

const MarvelConfig = apiConfigs[0]['api-configs'][0].marvel;
const _ts = (() => new Date().getTime())();
const _PUBLIC_KEY = (() => MarvelConfig['public-key'])();
const _PRIVATE_KEY = (() => MarvelConfig['private-key'])();
const _hash = (() => Crypto.MD5(_ts+_PRIVATE_KEY+_PUBLIC_KEY).toString())();

const params = {
    ts: _ts,
    hash: _hash,
    apikey: _PUBLIC_KEY
}

const CreateApi = Axios.create({
    baseURL: MarvelConfig['base-url'],
    params,
})

CreateApi.interceptors.request.use(function (config) {
    store.commit('switch');
    return config;
  }, function (error) {
    return Promise.reject(error);
  });

export {CreateApi};
