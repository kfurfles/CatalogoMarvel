import {CreateApi} from './marvel-create-api';
/*

/v1/public/characters Fetches lists of characters.
GET /v1/public/characters/{characterId} Fetches a single character by id.
GET /v1/public/characters/{characterId}/comics Fetches lists of comics filtered by a character id.
GET /v1/public/characters/{characterId}/events Fetches lists of events filtered by a character id.
GET /v1/public/characters/{characterId}/series Fetches lists of series filtered by a character id.
GET /v1/public/characters/{characterId}/stories Fetches lists of stories filtered by a character id.

*/

const getCharacter = () => CreateApi.get('/v1/public/characters', {
                             params: {
                                 limit: 50,
                                 offset: 0,
                             },
                         });

export {getCharacter};
