import {uniqBy} from 'lodash';

export default{
    switch(state) {
        state.showLoader = !state.showLoader;
    },
    ADD_DREAM_TEAM(state, newCharacter) {
        state.dreamTeam.push(newCharacter);
        state.dreamTeam = uniqBy(state.dreamTeam, 'id');
    },
};
