/* eslint-disable */
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import ProgressBar from '@/components/loader/Loader'

import Vue from 'vue';
import App from './App';
import {routes} from './js/routes/routes';
import { MarvelKey } from './key';
import $ from "jquery";
import * as svgicon from 'vue-svgicon'

import Vuex from 'vuex'
Vue.use(Vuex)

import storePlugin from '@/store'
Vue.use(storePlugin)
Vue.use(VueResource);
Vue.use(VueRouter);
Vue.use(svgicon, {
    tagName: 'svgicon'
})

const bar = Vue.prototype.$bar = new Vue(ProgressBar).$mount()
document.body.appendChild(bar.$el)

import '@/assets/font-awesome/css/fa-svg-with-js.css';
import '@/assets/font-awesome/js/fontawesome-all.min.js';

Vue.config.productionTip = false;

const router = new VueRouter({
  routes,
  mode: 'history',
});

export const apiConfig = {
    PUBLIC_KEY: '138fa6ccb30f9aa22051dfdc9a509a40',
    PRIVATE_KEY: '20fc8de49632f9a342b6add30e4bdbacac9fe3d9',
    ts: new Date().getTime(),
};

Vue.http.options.root = 'https://gateway.marvel.com:443';
Vue.http.options.marvel = 'https://gateway.marvel.com:443';


export default new Vue({
    el: '#app',
    router,
    render: (h) => h(App),
});

