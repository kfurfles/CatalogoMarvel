/* eslint-disable */
import {MarvelKey} from '@/key';

export default class Personagens {
    constructor(CryptoJs = '', resource) {
        this._PUBLIC_KEY = MarvelKey.publicKey;
        this._PRIVATE_KEY = MarvelKey.privateKey;
        this._ts = new Date().getTime();
        this._CryptoJs = CryptoJs;
        this.hash = this._CryptoJs.MD5(this._ts + this._PRIVATE_KEY + this._PUBLIC_KEY).toString();
        this._resource = resource;
        this.data = {
            params: {
                ts: this._ts,
                hash: this.hash,
                apikey: this._PUBLIC_KEY,
            },
        };
    }
    lista(offset, limit) {
        this.data.params.limit = limit;
        this.data.params.offset = offset;
        return this._resource.get(`${this._resource.options.root}/v1/public/characters`, this.data)
            .then(res => res.body.data, err => err = false);
    }
}

