// eslint-disable-next-line
import Home from '@/layouts/dashboard/Dashboard';
import Personagem from '@/layouts/personagem/Personagem';
import Persongens from '@/layouts/personagens/Personagens';
import Revistas from '@/layouts/revistas/Revistas';
import Escritores from '@/layouts/criadores/Criadores';
import Eventos from '@/layouts/eventos/Eventos';

export const routes = [
    {path: '*', component: Home, menu: false},
    {path: '/perfil', name: 'Perfil', component: Home, menu: false, controlUser: true},
    {path: '/logout', name: 'Logout', component: Home, menu: false, controlUser: true},
    {path: '/personagem/:id', name: 'Personagem', component: Personagem, menu: false, controlUser: false},
    {path: '/personsagens', name: 'Personagens', component: Persongens, menu: true, controlUser: false},
    {path: '/revistas', name: 'Revistas', component: Revistas, menu: true, controlUser: false},
    {path: '/escritores', name: 'Escritores', component: Escritores, menu: true, controlUser: false},
    {path: '/eventos', name: 'Eventos', component: Eventos, menu: true, controlUser: false},
];
